const inDevMode = false;

const url = inDevMode
? 'http://localhost:8888/FeelSummerApiProxy'
: 'https://proxy.lloydhesketh.co.uk';

export default {
  GET_EVENTS: `${url}/getProducts.php`,
  GET_CUSTOMERS: `${url}/getCustomers.php`,

  STRIPE_CREATE_CHARGE: `${url}/stripeCreateCharge.php`,
  STRIPE_CREATE_CUSTOMER: `${url}/stripeCreateCustomer.php`,
  STRIPE_CREATE_CUSTOMER_CHARGE: `${url}/stripeCreateCustomerCharge.php`,

  API_KEY: 'e63a8061-4372-4b01-90a1-f1181a2e267e',
};
