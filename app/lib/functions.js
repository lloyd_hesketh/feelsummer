import numeral from 'numeral';

export default {
  stripHTML(value) {
    return value.replace(/<(?:.|\n)*?>/gm, '');
  },
  toCurrency(value) {
    return `£${numeral(Number.parseInt(value, 10)).format('0,0.00')}`;
  },
};
