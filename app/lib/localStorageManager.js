import {
  AsyncStorage,
} from 'react-native';

const APP_KEY = '@FeelSummer:';

export function getStorageItem(key) {
  return AsyncStorage.getItem(`${APP_KEY}${key}`)
    .then(resp => JSON.parse(resp));
}

export function setStorageItem(key, value) {
  return AsyncStorage.setItem(`${APP_KEY}${key}`, JSON.stringify(value))
    .then(resp => resp);
}

export function deleteStorageItem(key) {
  return AsyncStorage.removeItem(`${APP_KEY}${key}`)
    .then(resp => resp);
}

export function updateStorageItem(key, value) {
  return AsyncStorage.mergeItem(`${APP_KEY}${key}`, JSON.stringify(value))
    .then(resp => resp);
}
