import React from 'react';

import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

import colors from '../lib/colors.json';
import text from '../lib/text.json';
import NavigationBar from '../components/navigationBar';
import PaddedScrollView from '../components/paddedScrollView';
import PageTitle from '../components/pageTitle';

const ScreenWidth = Dimensions.get('window').width;

const Guide = function Guide(props) {
  return (
    <View style={styles.wrapper}>
      <NavigationBar text={props.title} backgroundColor={colors.sea} />
      <PaddedScrollView>
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={require('../../images/guide-image.png')} />
        </View>
        <View style={styles.imageBreak} />
        <PageTitle title="Guide" />
        <Text style={styles.text}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut neque urna, porta a lacus at, viverra aliquet orci. Mauris interdum aliquet tristique. Ut ut sem pharetra, consequat sapien quis, tristique nibh. Donec tempor erat tellus, vel pellentesque nibh dictum nec. Maecenas sodales tincidunt tincidunt. Donec at purus suscipit, elementum orci a, tempor mi. Nulla congue tellus arcu, nec venenatis enim fermentum eget.

        Maecenas venenatis est erat, ac pharetra mauris ullamcorper nec. Curabitur rhoncus sollicitudin mauris a tincidunt. In convallis arcu quis elit molestie, a volutpat magna porta. Nunc sed accumsan dolor, placerat feugiat nibh. In iaculis neque ut leo porttitor, eu porta dui dignissim. Sed sit amet nunc urna. Praesent vitae nisl hendrerit, bibendum augue ac, imperdiet magna. In in dignissim massa. Vivamus vitae leo ultricies turpis euismod placerat. Duis ultricies dui pellentesque tempus vulputate. Cras cursus, nibh a condimentum volutpat, leo nisi varius enim, ut sodales ipsum velit sit amet velit. Praesent vel lectus vulputate, sagittis enim eget, dictum est. In luctus in dui et vehicula. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque pulvinar, sapien ut elementum ultrices, nisl magna interdum arcu, et rutrum nisl sapien ut dolor.

        Aliquam elementum vestibulum odio quis vestibulum. Etiam risus quam, bibendum et enim non, convallis ultricies augue. Phasellus auctor nunc vitae lorem maximus tristique. Duis nec accumsan massa, et aliquam quam. Fusce egestas neque vel tincidunt porttitor. Fusce vel metus nibh. Suspendisse vulputate velit et orci semper, ut luctus odio varius. Fusce faucibus felis id est consectetur efficitur.

        Vivamus quis arcu iaculis turpis consectetur scelerisque. Sed dictum sapien dignissim sapien placerat mattis. Aliquam non ante semper, mattis dui vel, cursus turpis. Aliquam erat volutpat. Morbi ac sodales turpis. Praesent ac porttitor nisl. In ultrices in tortor ut ornare.

        Duis sodales ultrices tristique. Sed sed posuere risus, vel bibendum enim. Sed luctus accumsan erat id euismod. Aliquam condimentum nisi in vehicula laoreet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc dapibus dapibus elit quis ultricies. Nullam sed eros a odio faucibus sodales. Mauris sed pulvinar purus. Nulla vel mollis nisi. Nam posuere rutrum odio et hendrerit. Ut porta purus sed nisl accumsan, non bibendum felis dictum. Vestibulum volutpat maximus urna, ac sollicitudin tortor ultrices vel. Maecenas vehicula elit felis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec efficitur tempus diam id ullamcorper. Maecenas bibendum erat id odio tempor mattis.
        </Text>
      </PaddedScrollView>
    </View>
  );
};

Guide.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Guide;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.lightGrey,
  },
  imageContainer: {
    height: 200,
  },
  image: {
    flex: 1,
    width: ScreenWidth,
  },
  imageBreak: {
    height: 5,
  },
  text: {
    ...text,
    padding: 5,
    textAlign: 'center',
    fontSize: 15,
  },
});
