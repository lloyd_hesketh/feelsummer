import React from 'react';

import {
  View,
  Text,
  StyleSheet,
  
} from 'react-native';

const TermsAndConditions = function TermsAndConditions() {
  return (
    <View style={styles.wrapper}>
      
      <Text>T&C&apos;s</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: 100,
  },
});

export default TermsAndConditions;
