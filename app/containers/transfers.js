import React from 'react';

import {
  View,
  StyleSheet,

} from 'react-native';
import PropTypes from 'prop-types';

import colors from '../lib/colors.json';
import NavigationBar from '../components/navigationBar';

const Transfers = function Transfers(props) {
  return (
    <View style={styles.wrapper}>

      <NavigationBar text={props.title} backgroundColor={colors.sea} />
    </View>
  );
};

Transfers.propTypes = {
  title: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
});

export default Transfers;
