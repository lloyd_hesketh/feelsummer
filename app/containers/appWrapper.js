import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import reducers from '../reducers';
import App from './app';

// create store...
const middleware = [thunk];
const store = compose(
  applyMiddleware(...middleware),
)(createStore)(reducers);

const AppWrapper = function AppWrapper() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default AppWrapper;
