import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ListView,
  TouchableHighlight,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import stripe from 'tipsi-stripe';
import Modal from 'react-native-modal';
import t from 'tcomb-form-native';

import * as checkoutActions from '../actions/checkoutActions';

import colors from '../lib/colors.json';
import text from '../lib/text.json';
import NavigationBar from '../components/navigationBar';
import PaddedScrollView from '../components/paddedScrollView';
import LoadingSpinner from '../components/loadingSpinner';
import ModalButton from '../components/viewEvent/modalButton';

const Form = t.form.Form;

const EmailValidation = t.subtype(t.Str, s => /.+@.+\..+/.test(s));


const BillingDetails = t.struct({
  email: EmailValidation,              // a required string
  address1: t.String,  // an optional string
  address2: t.maybe(t.String),              // a required number
  rememberMyDetails: t.Boolean,        // a boolean
});

const formOptions = {};

class StripeCheckout extends Component {
  componentDidMount() {
    this.props.checkoutActions.initialiseCheckout();
  }
  onEnterCardDetailsClick = () => {
    const formData = this.form.getValue();
    if (formData) {
      this.props.checkoutActions.updateBillingDetails(formData);
      stripe.init({
        publishableKey: 'pk_test_CBU73kYXeah5xQwddoDCa6uI',
      });
      const dialogOptions = {
        smsAutofillDisabled: true,
        theme: {
          primaryBackgroundColor: colors.lightGrey,
          accentColor: colors.pink,
        },
        billingAddress: {
          name: formData.email,
          line1: formData.address1,
          line2: formData.address2,
          email: formData.email,
        }
      };
      async function createStripeToken(cb) {
        const data = await stripe.paymentRequestWithCardForm(dialogOptions);
        cb(data);
      }
      createStripeToken((stripeData) => {
        this.props.checkoutActions.updateStripeToken(stripeData);
        if (formData.rememberMyDetails) {
          this.props.checkoutActions.stripeCreateCustomer(stripeData, formData.email);
        }
      })
      .catch((err) => {

      });
    }
  }
  onConfirmPaymentPress = () => {
    if (this.props.billingDetails) {
      this.props.checkoutActions.updateCheckoutStatus('loading');
      if (this.props.billingDetails.rememberMyDetails) {
        this.props.checkoutActions.stripeCreateCustomerCharge(
          { tokenId: this.props.stripeCustomerDetails.id },
          this.props.cartTotal * 100,
          this.props.billingDetails.email,
          this.props.rawCartItems,
        );
      } else {
        this.props.checkoutActions.stripeCreateCharge(
          this.props.stripeCheckoutToken,
          this.props.cartTotal * 100,
          this.props.billingDetails.email,
          this.props.rawCartItems,
        );
      }
    }
  }
  render() {
    console.log('checkout render', this.props);
    return (
      <View style={styles.wrapper}>
        <NavigationBar text={this.props.title} {...this.props} />
        <View style={styles.contentWrapper}>
          <View style={styles.container}>
            {
              this.props.stripeCheckoutToken != null || this.props.stripeCustomerDetails != null
              ? <View><TouchableHighlight style={styles.button} onPress={this.onConfirmPaymentPress} underlayColor="#99d9f4">
                <Text style={styles.buttonText}>Confirm Payment</Text>
              </TouchableHighlight></View>
              : <View><Form
                ref={(input) => { this.form = input; }}
                type={BillingDetails}
                value={this.props.billingDetails}
                options={formOptions}
              /><TouchableHighlight
                style={styles.button}
                onPress={this.onEnterCardDetailsClick}
              >
                <Text style={styles.buttonText}>Enter Card Details</Text>
              </TouchableHighlight></View>
            }

          </View>
          <Modal
              isVisible={this.props.stripeCheckoutStatus === 'loading' || this.props.stripeCheckoutStatus === 'error'}
              backdropOpacity={0.87}
            >
            {
              this.props.stripeCheckoutStatus === 'loading'
              ? <View style={{flex: 1, }}>
                <View style={{flex: 5,}} />
                <View style={{flex: 5,}}>
                  <LoadingSpinner size={100} type="9CubeGrid" color={colors.pink} />
                </View>
                <View style={{flex: 5, alignItems: 'center'}}>
                  <Text style={styles.processingText}>Processing Payment...</Text>
                </View>
              </View>
              : <View style={{alignItems: 'center'}}>
                <Text style={styles.processingText}>There has been an error processing your payment.</Text>
                <ModalButton onPress={() => this.props.checkoutActions.updateCheckoutStatus(null)}/>
              </View>
            }
          </Modal>
        </View>
      </View>
    );
  }
}

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default connect(state => ({
  rawCartItems: state.cart.items,
  cartItems: ds.cloneWithRows(state.cart.items),
  cartTotal: state.cart.total,
  stripeCheckoutStatus: state.stripeCheckout.status,
  stripeCheckoutToken: state.stripeCheckout.stripeToken,
  billingDetails: state.stripeCheckout.billingDetails,
  stripeCustomerDetails: state.stripeCheckout.savedStripeCard,
}),
dispatch => ({
  // cartActions: bindActionCreators(cartActions, dispatch),
  checkoutActions: bindActionCreators(checkoutActions, dispatch),
}),
)(StripeCheckout);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.lightGrey,
  },
  contentWrapper: {
    flex: 8,
  },
  checkoutWrapper: {
    flex: 2,
  },
  processingText: {
    ...text,
    color: 'white',
  },
  container: {
    justifyContent: 'center',
    marginTop: 50,
    padding: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    ...text,
    fontSize: 30,
    alignSelf: 'center',
    marginBottom: 30,
  },
  buttonText: {
    ...text,
    fontSize: 18,
    color: 'white',
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  button: {
    height: 36,
    backgroundColor: colors.pink,
    // borderColor: '#48BBEC',
    // borderWidth: 1,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
});
