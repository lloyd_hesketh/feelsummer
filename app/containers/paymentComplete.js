import React, { Component } from 'react';
import {
  View,
  Text,
} from 'react-native';

class PaymentComplete extends Component {
  render() {
    return (
      <View>
        <Text>Payment Complete</Text>
      </View>
    );
  }
}

export default PaymentComplete;
