import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as counterActions from '../actions/counterActions';
import Home from './home';

const FeelSummer = function FeelSummer() {
  return (<Home />);
};

export default connect(() => ({

}),
dispatch => ({
  actions: bindActionCreators(counterActions, dispatch),
}),
)(FeelSummer);
