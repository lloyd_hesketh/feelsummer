import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
  Text,
  Picker,
} from 'react-native';

import Modal from 'react-native-modal';
import CalendarPicker from 'react-native-calendar-datepicker';
import Moment from 'moment';

import * as viewEventActions from '../actions/viewEventActions';

import colors from '../lib/colors.json';

import NavigationBar from '../components/navigationBar';
import PaddedScrollView from '../components/paddedScrollView';
import Image from '../components/viewEvent/image';
import Title from '../components/viewEvent/title';
import PurchaseArea from '../components/viewEvent/purchase';
import ModalButton from '../components/viewEvent/modalButton';
import Calendar from '../components/calendar';
import InfoTitle from '../components/viewEvent/infoTitle';


class ViewEvent extends Component {
  componentWillUnmount = () => {
    this.props.actions.resetState();
  }
  getPickerItems = () => {
    const listOfPickerItems = [];
    for (let i = 1; i <= 20; i += 1) {
      listOfPickerItems.push(
        <Picker.Item key={`PickerItem${i}`} label={i.toString()} value={i} />
      );
    }
    return listOfPickerItems;
  }
  render() {
    console.log(this.props);
    return (
      <View style={styles.wrapper}>
        <NavigationBar text={this.props.event.name} backgroundColor={colors.pink} />
        <PaddedScrollView>
          <Image src={this.props.event.images[0].src} />
          <Title text={this.props.event.name} />
          <PurchaseArea
            event={this.props.event}
            selectedDate={this.props.confirmedDate}
            selectedQuantity={this.props.confirmedQuantity}
          />
          <InfoTitle text="WHATS INCLUDED" />
          <InfoTitle text="TICKET INFO" />
          <InfoTitle text="TICKET SHOP INFO" />
        </PaddedScrollView>
        <Modal
          isVisible={this.props.showDateModal}
          onBackdropPress={this.props.actions.toggleDateModal}
          backdropOpacity={0.87}
        >
          <Calendar
            selectedDate={this.props.selectedDate}
            onChange={this.props.actions.setSelectedDate}
          />
          <ModalButton onPress={() => this.props.actions.confirmDate(this.props.selectedDate)}/>
        </Modal>
        <Modal
          isVisible={this.props.showQuantityModal}
          onBackdropPress={this.props.actions.toggleQuantityModal}
          backdropOpacity={0.87}
        >
          <View style={styles.pickerWrapper}>
            <Picker
              selectedValue={this.props.selectedQuantity}
              onValueChange={itemValue => this.props.actions.setSelectedQuantity(itemValue)}
            >
              {this.getPickerItems()}
            </Picker>
            <ModalButton
              onPress={() => this.props.actions.confirmQuantity(this.props.selectedQuantity)}
            />
          </View>
        </Modal>
      </View>
    );
  }
}

export default connect(state => ({
  showDateModal: state.viewEvent.showDateModal,
  showQuantityModal: state.viewEvent.showQuantityModal,
  selectedDate: state.viewEvent.selectedDate,
  confirmedDate: state.viewEvent.confirmedDate,
  selectedQuantity: state.viewEvent.selectedQuantity,
  confirmedQuantity: state.viewEvent.confirmedQuantity,
  cart: state.cart,
}),
dispatch => ({
  actions: bindActionCreators(viewEventActions, dispatch),
}),
)(ViewEvent);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  callendar: {
    backgroundColor: '#FFF',
  },
  daySelectedText: {
    backgroundColor: colors.green,
    borderRadius: 15,
    borderColor: 'transparent',
    color: '#000',
  },
  dayText: {
    color: colors.grey,
  },
  pickerWrapper: {
    backgroundColor: '#FFF',
  },
});
