import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  View,
  StyleSheet,
  Text,
  ListView,
} from 'react-native';
import PropTypes from 'prop-types';

import * as bookEventActions from '../actions/bookEventActions';

import colors from '../lib/colors.json';
import NavigationBar from '../components/navigationBar';
import PaddedScrollView from '../components/paddedScrollView';
import EventTile from '../containers/eventTile';
import LoadingSpinner from '../components/loadingSpinner';
import Retry from '../components/retry';

class BookEvents extends Component {
  componentDidMount() {
    if (this.props.eventsLoaded !== true) {
      this.props.actions.getEvents();
    }
  }
  componentWillUnmount() {
    this.props.actions.handleBookEventsUnmount();
  }
  retryGetEvents = () => {
    this.props.actions.handleBookEventsUnmount();
    this.props.actions.getEvents();
  }
  render() {
    console.log('bookEvent.js', this.props);
    let componentToRender = null;
    switch (this.props.eventsLoaded) {
      case true:
        componentToRender = (
          <PaddedScrollView>
            <ListView
              dataSource={this.props.events}
              renderRow={rowData => <EventTile data={rowData} />}
            />
          </PaddedScrollView>
        );
        break;
      case false:
        componentToRender = (<Retry onPress={this.retryGetEvents}/>);
        break;
      default:
        componentToRender = <LoadingSpinner size={50} type="9CubeGrid" color={colors.pink} />
    }
    return (
      <View style={styles.wrapper}>
        <NavigationBar text={this.props.title} backgroundColor={colors.pink} />
        {componentToRender}
      </View>
    );
  }
}

BookEvents.propTypes = {
  title: PropTypes.string.isRequired,
};

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default connect(state => ({
  events: ds.cloneWithRows(state.bookEvent.events),
  eventsLoaded: state.bookEvent.eventsLoaded,
  eventCount: state.bookEvent.eventCount,
}),
dispatch => ({
  actions: bindActionCreators(bookEventActions, dispatch),
}),
)(BookEvents);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.lightGrey
  },
});
