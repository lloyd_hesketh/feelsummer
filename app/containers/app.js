// app.js
import React from 'react';
import { Router, Scene } from 'react-native-router-flux';

import FeelSummer from './feelSummer';
import BookEvents from './bookEvents';
import ViewEvent from './viewEvent';
import TicketWallet from './ticketWallet';
import Transfers from './transfers';
import Guide from './guide';
import Cart from './cart';
import Checkout from './stripeCheckout';
import PaymentComplete from './paymentComplete';

import TermsAndConditions from './termsAndConditions';
import PrivacyPolicy from './privacyPolicy';

const App = function App() {
  return (
    <Router>
      <Scene key="root">
        <Scene key="home" component={FeelSummer} title="" initial hideNavBar />
        <Scene key="bookEvents" component={BookEvents} title="Book Events" hideNavBar />
        <Scene key="viewEvent" component={ViewEvent} title="View Event" hideNavBar />
        <Scene key="ticketWallet" component={TicketWallet} title="Ticket Wallet" hideNavBar />
        <Scene key="transfers" component={Transfers} title="Transfers" hideNavBar />
        <Scene key="guide" component={Guide} title="Guide" hideNavBar />
        <Scene key="cart" component={Cart} title="Cart" hideNavBar />
        <Scene key="checkout" component={Checkout} title="Checkout" hideNavBar />
        <Scene key="paymentComplete" component={PaymentComplete} title="Payment Complete" hideNavBar />

        <Scene key="termsAndConditions" component={TermsAndConditions} title="Terms And Conditions" hideNavBar={false} direction="vertical" />
        <Scene key="privacyPolicy" component={PrivacyPolicy} title="Privacy Policy" hideNavBar={false} direction="vertical" />
      </Scene>
    </Router>
  );
};

export default App;
