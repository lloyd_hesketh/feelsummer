import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import {
  View,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import SettingsButton from '../components/settings/button';
import TitleBreaker from '../components/settings/titleBreaker';
import SettingsHeader from '../components/settings/header';
import SettingsContent from '../components/settings/content';
import TextWrapperButton from '../components/settings/textWrapperButton';

class SettingsDrawer extends Component {
  handleNavigationClick = (actionToCall) => {
    this.props.closeDrawer();
    actionToCall();
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <TextWrapperButton onClick={this.props.closeDrawer}>
          <SettingsButton text="HOME SCREEN" />
        </TextWrapperButton>
        <TextWrapperButton onClick={() => { this.handleNavigationClick(Actions.bookEvents); }}>
          <SettingsButton text="BOOK EVENTS" />
        </TextWrapperButton>
        <TextWrapperButton onClick={() => { this.handleNavigationClick(Actions.ticketWallet); }}>
          <SettingsButton text="MY E TICKETS" />
        </TextWrapperButton>
        <TextWrapperButton onClick={() => { this.handleNavigationClick(Actions.transfers); }}>
          <SettingsButton text="TRANSFERS" />
        </TextWrapperButton>
        <TextWrapperButton onClick={() => { this.handleNavigationClick(Actions.guide); }}>
          <SettingsButton text="MAGALUF GUIDE" />
        </TextWrapperButton>
        <TitleBreaker text="MY ACCOUNT" />
        <TextWrapperButton onClick={() => {}}>
          <SettingsHeader text="You're not logged in" />
          <SettingsContent text="Tap here to sign up or log in" />
        </TextWrapperButton>
        <TitleBreaker text="SETTINGS" />
        <TextWrapperButton onClick={() => {}}>
          <SettingsHeader text="My App Permissions" />
          <SettingsContent text="Enable Location and Event permissions" />
        </TextWrapperButton>
        <TextWrapperButton onClick={() => {}}>
          <SettingsHeader text="My App Settings" />
          <SettingsContent text="Enable Location and Event permissions" />
        </TextWrapperButton>
        <TitleBreaker text="MORE" />
        <TextWrapperButton onClick={Actions.termsAndConditions}>
          <SettingsHeader text="Terms And Conditions" />
        </TextWrapperButton>
        <TextWrapperButton onClick={Actions.privacyPolicy}>
          <SettingsHeader text="Privacy Policy" />
        </TextWrapperButton>
        <TextWrapperButton onClick={() => {}}>
          <SettingsHeader text="1.0.0" />
        </TextWrapperButton>
      </View>
    );
  }
}

SettingsDrawer.propTypes = {
  closeDrawer: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
    marginTop: 20,
  },
});

export default SettingsDrawer;
