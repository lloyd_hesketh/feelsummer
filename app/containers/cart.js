import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ListView,
  TouchableHighlight,
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import stripe from 'tipsi-stripe';
import { Actions } from 'react-native-router-flux';

import * as cartActions from '../actions/cartActions';

import colors from '../lib/colors.json';
import text from '../lib/text.json';
import functions from '../lib/functions';
import NavigationBar from '../components/navigationBar';
import PaddedScrollView from '../components/paddedScrollView';
import CartItem from '../components/cart/cartItem';

class Cart extends Component {
  onPayClick = () => {
    Actions.checkout();
  }
  removeItemFromCart = (index) => {
    this.props.cartActions.removeCartItemAtIndex(index);
  }
  render() {
    return (
      <View style={styles.wrapper}>
        <NavigationBar text={this.props.title} {...this.props} />
        <View style={styles.contentWrapper}>
          <PaddedScrollView>
            <ListView
              dataSource={this.props.cartItems}
              renderRow={(rowData, listID, index) =>
                <CartItem
                  data={rowData}
                  index={Number.parseInt(index, 10)}
                  removeItemClick={this.removeItemFromCart}
                />
              }
            />
          </PaddedScrollView>
        </View>
        <View style={styles.checkoutWrapper}>
          <Text>Checkout</Text>
          <Text>Total: {functions.toCurrency(this.props.cartTotal)}</Text>
          <TouchableHighlight
            onPress={this.onPayClick}
          >
            <Text>Checkout</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

// export default Cart;

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default connect(state => ({
  cartItems: ds.cloneWithRows(state.cart.items),
  cartTotal: state.cart.total,
}),
dispatch => ({
  cartActions: bindActionCreators(cartActions, dispatch),
  // checkoutActions: bindActionCreators(checkoutActions, dispatch),
}),
)(Cart);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.lightGrey,
  },
  contentWrapper: {
    flex: 8,
  },
  checkoutWrapper: {
    flex: 2,
  },
});
