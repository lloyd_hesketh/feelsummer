import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  ListView,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import NavigationBar from '../components/navigationBar';
import colors from '../lib/colors.json';
import PaddedScrollView from '../components/paddedScrollView';


class TicketWallet extends Component {
  render() {
    console.log('ticket wallet', this.props);
    return (
      <View style={styles.wrapper}>
        <NavigationBar text={this.props.title} backgroundColor={colors.sea} />
        <PaddedScrollView>
          <ListView
            dataSource={this.props.walletItems}
            renderRow={(rowData, listID, index) => {
              console.log(rowData);
              return <Text>Item</Text>
            }}
          />
        </PaddedScrollView>
      </View>
    );
  }
}

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

export default connect(state => ({
  walletItems: ds.cloneWithRows(state.myTickets.items),
}),
dispatch => ({
  // checkoutActions: bindActionCreators(checkoutActions, dispatch),
}),
)(TicketWallet);

TicketWallet.propTypes = {
  title: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.lightGrey,
  },
});

// export default TicketWallet;
