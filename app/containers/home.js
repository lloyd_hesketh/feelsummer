import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  View,
  StyleSheet,
} from 'react-native';

import Drawer from 'react-native-drawer';

import * as cartActions from '../actions/cartActions';
import * as myTicketsActions from '../actions/myTicketsActions';

import colors from '../lib/colors.json';
import Header from '../components/home/header';
import Menu from '../components/home/menu';
import SettingsMenu from '../containers/settingsMenu';
import CartCheckout from '../components/home/checkout';

class Home extends Component {
  componentDidMount() {
    this.props.cartActions.initialiseCart();
    this.props.myTicketsActions.initialiseMyTickets();
  }
  closeControlPanel = () => {
    this.drawer.close();
  }
  openControlPanel = () => {
    this.drawer.open();
  }
  render() {
    console.log('home.js', this.props);
    return (
      <Drawer
        ref={ref => (this.drawer = ref)}
        type="overlay"
        content={<SettingsMenu closeDrawer={this.closeControlPanel} />}
        tapToClose
        openDrawerOffset={0.2}
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        styles={drawerStyles}
        tweenHandler={ratio => ({
          main: { opacity: (2 - ratio) / 2 },
        })}
      >
        <View style={styles.wrapper}>
          <Header style={styles.header} openDrawer={this.openControlPanel} />
          <View style={styles.main} />
          <Menu style={styles.menu} />
          {
            this.props.cart.items.length > 0
            ? <CartCheckout totalItemsInCart={this.props.cart.items.length} />
            : null
          }
        </View>
      </Drawer>
    );
  }
}

export default connect(state => ({
  cart: state.cart,
}),
dispatch => ({
  cartActions: bindActionCreators(cartActions, dispatch),
  myTicketsActions: bindActionCreators(myTicketsActions, dispatch),
}),
)(Home);


const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: colors.lightGrey,
  },
  header: {
    flex: 2,
  },
  main: {
    flex: 10,
  },
  menu: {
    flex: 4,
  },
  cart: {
    flex: 2,
  },
});

const drawerStyles = {
  drawer: { },
  main: { paddingLeft: 3 },
};
