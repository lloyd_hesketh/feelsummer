import React from 'react';

import {
  View,
  Text,
  StyleSheet,
  
} from 'react-native';

const PrivacyPolicy = function PrivacyPolicy() {
  return (
    <View style={styles.wrapper}>
      
      <Text>Privacy Policy</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop: 100,
  },
});

export default PrivacyPolicy;
