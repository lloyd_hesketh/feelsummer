import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import EventTileImage from '../components/eventTile/image';
import EventTileInfo from '../components/eventTile/info';
import EventTilePurchase from '../components/eventTile/purchase';

class EventTile extends Component {
  handleBookNowPress = () => {
    Actions.viewEvent({ event: this.props.data });
  }
  render() {
    console.log(this.props.data);
    return (
      <View style={styles.wrapper}>
        <EventTileImage
          src={this.props.data.images[0] !== null ? this.props.data.images[0].src : null}
        />
        <EventTileInfo
          name={this.props.data.name}
          description={this.props.data.short_description}
        />
        <EventTilePurchase
          price={this.props.data.price}
          id={this.props.data.id}
          handleBookNowPress={this.handleBookNowPress}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'stretch',
    margin: 10,
  },
});

export default EventTile;
