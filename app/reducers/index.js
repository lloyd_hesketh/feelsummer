// reducers/index.js

import { combineReducers } from 'redux';
import routes from './routes';
import bookEvent from './bookEvent';
import viewEvent from './viewEvent';
import cart from './cart';
import myTickets from './myTickets';
import stripeCheckout from './stripeCheckout';
// ... other reducers

export default combineReducers({
  routes,
  bookEvent,
  viewEvent,
  cart,
  myTickets,
  stripeCheckout,
  // ... other reducers
});
