import * as types from '../actions/actionTypes';
import {
  setStorageItem,
} from '../lib/localStorageManager';

const CART_KEY = 'cart';

const initialState = {
  items: [],
  total: 0,
};

const calculateCartTotal = (items) => {
  let total = 0;
  for (let i = 0; i < items.length; i += 1) {
    const item = items[i];
    total += item.event.price * item.quantity;
  }
  return total;
};

export default function cart(state = initialState, action = {}) {
  switch (action.type) {
    case types.INITIALISE_CART: {
      if (action.payload) {
        return {
          ...action.payload,
        };
      }
      return {
        ...initialState,
      };
    }
    case types.ADD_EVENT_TO_CART: {
      const newState = { ...state };
      newState.items.push(action.payload);
      newState.total = calculateCartTotal(newState.items);
      setStorageItem(CART_KEY, newState);
      return {
        ...state,
        ...newState,
      };
    }
    case types.REMOVE_EVENT_FROM_CART: {
      const newState = { ...state };
      newState.items.splice(action.payload, 1);
      newState.total = calculateCartTotal(newState.items);
      setStorageItem(CART_KEY, newState);
      return {
        ...state,
        ...newState,
      };
    }
    case types.STRIPE_CHARGE_SUCCESS: {
      const newState = { ...initialState };
      setStorageItem(CART_KEY, newState);
      return {
        ...newState,
      };
    }
    default:
      return state;
  }
}
