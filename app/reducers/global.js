import * as types from '../actions/actionTypes';

const initialState = {
  cart: {},
};

export default function global(state = initialState, action = {}) {
  switch (action.type) {
    case types.INITIALISE_CART:
      return {
        ...state,
        cart: action.payload,
      };
    default:
      return state;
  }
}
