import * as types from '../actions/actionTypes';
import {
  setStorageItem,
} from '../lib/localStorageManager';

const STRIPE_CUSTOMER_KEY = 'stripeCustomerData';

const initialState = {
  status: null,
  stripeToken: null,
  billingDetails: {
    email: 'test@test.com',
    address1: 'test',
    address2: '',
    rememberMyDetails: true,
  },
  savedStripeCard: null,
};

export default function stripeCheckout(state = initialState, action = {}) {
  switch (action.type) {
    case types.CHANGE_CHECKOUT_STATUS: {
      const newState = { ...state };
      newState.status = action.payload;
      return {
        ...newState,
      };
    }
    case types.UPDATE_STRIPE_TOKEN: {
      const newState = { ...state };
      newState.stripeToken = action.payload;
      return newState;
    }
    case types.UPDATE_BILLING_DETAILS: {
      const newState = { ...state };
      newState.billingDetails = { ...action.payload };
      return newState;
    }
    case types.SAVE_STRIPE_CUSTOMER_DATA: {
      const newState = { ...state };
      setStorageItem(STRIPE_CUSTOMER_KEY, action.payload);
      newState.savedStripeCard = action.payload;
      return newState;
    }
    default:
      return state;
  }
}
