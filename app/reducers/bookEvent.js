import * as types from '../actions/actionTypes';

const initialState = {
  events: [],
  eventsLoaded: null,
  eventCount: 0,
};

export default function bookEvents(state = initialState, action = {}) {
  switch (action.type) {
    case types.GET_EVENTS_SUCCESS:
      return {
        ...state,
        events: action.data,
        eventsLoaded: true,
        eventCount: action.data.length,
      };
    case types.GET_EVENTS_FAILURE:
      return {
        ...state,
        error: action.data,
        eventsLoaded: false,
      };
    case types.BOOK_EVENTS_UNMOUNT:
      return {
        ...state,
        eventsLoaded: null,
      };
    default:
      return state;
  }
}
