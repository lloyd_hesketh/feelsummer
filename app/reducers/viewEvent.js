import moment from 'moment';
import * as types from '../actions/actionTypes';

const initialState = {
  showDateModal: false,
  showQuantityModal: false,
  selectedDate: moment(),
  confirmedDate: moment(),
  selectedQuantity: 1,
  confirmedQuantity: 1,
};

export default function viewEvent(state = initialState, action = {}) {
  switch (action.type) {
    case types.RESET_VIEWEVENT_STATE:
      return {
        ...initialState,
      };
    case types.TOGGLE_DATE_MODAL:
      return {
        ...state,
        showDateModal: !state.showDateModal,
      };
    case types.TOGGLE_QUANTITY_MODAL:
      return {
        ...state,
        showQuantityModal: !state.showQuantityModal,
      };
    case types.SET_SELECTED_DATE:
      return {
        ...state,
        selectedDate: action.payload,
      };
    case types.SET_SELECTED_QUANTITY:
      return {
        ...state,
        selectedQuantity: action.payload,
      };
    case types.SET_CONFIRMED_DATE:
      return {
        ...state,
        confirmedDate: action.payload,
        showDateModal: false,
      };
    case types.SET_CONFIRMED_QUANTITY:
      return {
        ...state,
        confirmedQuantity: action.payload,
        showQuantityModal: false,
      };
    default:
      return state;
  }
}
