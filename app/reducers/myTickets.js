import * as types from '../actions/actionTypes';
import {
  setStorageItem,
} from '../lib/localStorageManager';

const MYTICKETS_KEY = 'myTickets';

const initialState = {
  items: [],
};

export default function myTickets(state = initialState, action = {}) {
  switch (action.type) {
    case types.INITIALISE_MYTICKETS: {
      if (action.payload) {
        return {
          ...action.payload,
        };
      }
      return {
        ...initialState,
      };
    }
    case types.SAVE_CART_TO_MY_TICKETS: {
      const newState = { ...state };
      newState.items = state.items.concat(action.payload);
      setStorageItem(MYTICKETS_KEY, newState);
      return {
        ...newState,
      };
    }
    default:
      return state;
  }
}
