import React from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import colors from '../lib/colors.json';
import text from '../lib/text.json';

const PageTitle = function PageTitle(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{props.title}</Text>
    </View>
  );
};

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
};

export default PageTitle;

let styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.sea,
    flexDirection: 'column',
    justifyContent: 'center',
    height: 70,
    padding: 10,
  },
  text: {
    ...text,
    fontSize: 20,
    color: 'white',
  },
});
