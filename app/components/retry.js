import React from 'react';

import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
} from 'react-native';

import colors from '../lib/colors.json';
import text from '../lib/text.json';

const Retry = function Retry(props) {
  return (
    <View style={styles.wrapper}>
      <TouchableHighlight
        onPress={props.onPress}
        style={styles.button}
      >
        <Text style={styles.text}>Retry...</Text>
      </TouchableHighlight>
    </View>
  );
};

export default Retry;

let styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: colors.pink,
    padding: 20,
  },
  text: {
    ...text,
    fontSize: 20,
    color: 'white',
  },
});
