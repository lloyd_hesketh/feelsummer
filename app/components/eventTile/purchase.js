import React from 'react';

import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';
import functions from '../../lib/functions';
import BookNowButton from './bookNowButton';

const EventTilePurchase = function EventTilePurchase(props) {
  return (
    <View style={styles.wrapper}>
      <View style={styles.priceWrapper}>
        <Text style={styles.priceText}>{functions.toCurrency(props.price)}</Text>
      </View>
      <View style={styles.bookWrapper}>
        <BookNowButton id={props.id} handlePress={props.handleBookNowPress}/>
      </View>
    </View>
  );
};

export default EventTilePurchase;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.grey,
    alignItems: 'center',
    flexDirection: 'row',
  },
  priceText: {
    ...text,
    color: '#fff',
    fontWeight: '800',
    fontSize: 35,
  },
  priceWrapper: {
    flex: 1,
    padding: 10,
    paddingLeft: 20,
  },
  bookWrapper: {
    flex: 1,
    padding: 10,
  },
});
