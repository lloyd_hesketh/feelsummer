import React, { Component } from 'react';

import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
} from 'react-native';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

class BookNowButton extends Component {
  render() {
    return (
      <TouchableHighlight onPress={this.props.handlePress}>
        <View style={styles.wrapper}>
          <Text style={styles.text}>{this.props.text || 'BOOK NOW'}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

export default BookNowButton;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.green,
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
  },
  text: {
    ...text,
    color: colors.grey,
    fontWeight: '800',
    fontSize: 20,
  },
});
