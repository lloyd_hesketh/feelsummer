import React from 'react';

import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';
import functions from '../../lib/functions';

const EventTileInfo = function EventTileInfo(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.nameText} numberOfLines={1}>{props.name.toUpperCase()}</Text>
      <Text style={styles.descText}>{functions.stripHTML(props.description)}</Text>
    </View>
  );
};

export default EventTileInfo;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.sea,
    alignItems: 'center',
  },
  nameText: {
    ...text,
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 20,
    padding: 10,
  },
  descText: {
    ...text,
    color: '#FFF',
    paddingLeft: 10,
    paddingRight: 10,
    textAlign: 'center',
  },
});
