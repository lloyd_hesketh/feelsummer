import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { CustomCachedImage } from 'react-native-img-cache';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

import colors from '../../lib/colors.json';

const EventTileImage = function EventTileImage(props) {
  const secureUri = props.src.replace('http://', 'https://');
  return (
    <View>
      <CustomCachedImage
        component={Image}
        source={{ uri: secureUri }}
        indicator={() => <ProgressBar color={colors.pink} />}
        style={styles.image}
        mutable
      />
    </View>
  );
};

export default EventTileImage;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 180,
  },
});
