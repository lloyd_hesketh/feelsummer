import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';

import Icon from 'react-native-vector-icons/FontAwesome';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

class Header extends Component {
  handleSettingsPress = () => {
    this.props.openDrawer();
  }
  render() {
    return (
      <View style={this.props.style}>
        <View style={styles.wrapper}>
          <View style={styles.logo}>
            <Text style={styles.logoText}>FEEL SUMMER</Text>
          </View>
          <TouchableHighlight
            style={styles.menu}
            onPress={this.handleSettingsPress}
            underlayColor="rgba(0,0,0,0)"
          >
            <Icon name="bars" size={30} color="#FFF" />
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

Header.propTypes = {
  openDrawer: PropTypes.func.isRequired,
  style: PropTypes.number,
};

Header.defaultProps = {
  style: {},
};

export default Header;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.pinkOpacity,
  },
  logo: {
    justifyContent: 'center',
    padding: 20,
  },
  logoText: {
    color: 'white',
    fontSize: 20,
    ...text,
    fontWeight: 'bold',
  },
  menu: {
    justifyContent: 'center',
    padding: 20,
  },
});
