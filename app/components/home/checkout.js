import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import ViewCheckoutButton from '../eventTile/bookNowButton';

class Checkout extends Component {
    onViewCheckoutPress = () => {
      Actions.cart();
    }
    render() {
      return (
        <View style={styles.cart}>
          <Text>{`${this.props.totalItemsInCart} ${this.props.totalItemsInCart > 1 ? 'Items' : 'Item'}`} in cart</Text>
          <ViewCheckoutButton handlePress={this.onViewCheckoutPress} text="View Cart" />
        </View>
      )
    }
}

export default Checkout;


const styles = StyleSheet.create({
  cart: {
    flex: 2,
    padding: 5,
    flexDirection: 'row',
  },
});
