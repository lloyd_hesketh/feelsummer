import React, { Component } from 'react';

import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';

import Icon from 'react-native-vector-icons/FontAwesome';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

export default class MenuButton extends Component {
  handlePress = () => {
    this.props.onClick();
  }
  render() {
    return (
      <TouchableHighlight
        style={styles.button}
        onPress={this.handlePress}
        underlayColor={colors.pink}
      >
        <View style={styles.content}>
          <Text style={styles.buttonText}>{this.props.text}</Text>
          <View style={styles.icon}>
            <Icon name={this.props.icon} size={this.props.iconSize} color="#FFF" />
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

MenuButton.propTypes = {
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  iconSize: PropTypes.number,
};

MenuButton.defaultProps = {
  iconSize: 30,
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
  },
  button: {
    backgroundColor: colors.pinkOpacity,
    flex: 1,
    margin: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 15,
    ...text,
    fontWeight: '800',
  },
  icon: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});
