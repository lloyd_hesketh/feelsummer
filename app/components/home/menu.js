import React from 'react';

import {
  View,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import { Actions } from 'react-native-router-flux';

import MenuButton from '../home/menuButton';

const HomeMenu = function PageTitle(props) {
  return (
    <View style={props.style}>
      <View style={styles.top}>
        <MenuButton text="BOOK EVENTS" icon="calendar" onClick={Actions.bookEvents} />
        <MenuButton text="MY E TICKETS" icon="ticket" onClick={Actions.ticketWallet} />
      </View>
      <View style={styles.bottom}>
        <MenuButton text="TRANSFERS" icon="car" iconSize={25} onClick={Actions.transfers} />
        <MenuButton text="QUICK GUIDE" icon="info-circle" iconSize={25} onClick={Actions.guide} />
      </View>
    </View>
  );
};

HomeMenu.propTypes = {
  style: PropTypes.number,
};

HomeMenu.defaultProps = {
  style: {},
};

export default HomeMenu;

const styles = StyleSheet.create({
  top: {
    flex: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  bottom: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
