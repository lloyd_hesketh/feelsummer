import React from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

const TitleBreaker = function TitleBreaker(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
};

TitleBreaker.propTypes = {
  text: PropTypes.string.isRequired,
};

export default TitleBreaker;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.lightGrey,
    paddingLeft: 5,
  },
  text: {
    ...text,
    fontSize: 25,
  },
});
