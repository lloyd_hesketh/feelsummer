import React from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import text from '../../lib/text.json';

const SettingsContent = function SettingsContent(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
};

SettingsContent.propTypes = {
  text: PropTypes.string,
};

SettingsContent.defaultProps = {
  text: '',
};

export default SettingsContent;

const styles = StyleSheet.create({
  wrapper: {
    paddingBottom: 3,
  },
  text: {
    ...text,
  },
});
