import React from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import text from '../../lib/text.json';

const SettingsHeader = function SettingsHeader(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
};

SettingsHeader.propTypes = {
  text: PropTypes.string.isRequired,
};

export default SettingsHeader;

const styles = StyleSheet.create({
  wrapper: {
    marginTop: 3,
  },
  text: {
    ...text,
    fontWeight: 'bold',
    fontSize: 20,
  },
});
