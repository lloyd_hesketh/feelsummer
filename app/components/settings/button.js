import React from 'react';

import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';

import text from '../../lib/text.json';


const SettingsButton = function SettingsButton(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
};

SettingsButton.propTypes = {
  text: PropTypes.string.isRequired,
};

export default SettingsButton;

const styles = StyleSheet.create({
  wrapper: {
    paddingTop: 2,
    paddingBottom: 2,
  },
  text: {
    ...text,
    fontSize: 25,
    fontWeight: 'bold',
  },
});
