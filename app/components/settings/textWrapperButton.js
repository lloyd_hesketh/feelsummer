import React, { Component } from 'react';

import {
  View,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';

import colors from '../../lib/colors.json';

export default class TextWrapperButton extends Component {
  handlePress = () => {
    this.props.onClick();
  }
  render() {
    return (
      <View>
        <TouchableHighlight
          style={styles.wrapper}
          onPress={this.handlePress}
          underlayColor={colors.pink}
        >
          <View style={styles.childrenWrapper}>
            {this.props.children}
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

TextWrapperButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node,
};

TextWrapperButton.defaultProps = {
  children: {},
};

const styles = StyleSheet.create({
  wrapper: {
  },
  childrenWrapper: {
    paddingLeft: 5,
  },
});
