import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

class CartItem extends Component {
  onDeletePress = () => {
    this.props.removeItemClick(this.props.index);
  }
  render() {
    console.log(this.props);
    return (
      <View style={styles.wrapper}>
        <Text>{this.props.index} {this.props.data.event.name}</Text>
        <TouchableHighlight
          onPress={this.onDeletePress}
        >
          <Text>Remove</Text>
        </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'stretch',
    margin: 10,
    flexDirection: 'row',
  },
});

export default CartItem;
