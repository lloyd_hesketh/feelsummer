import React from 'react';

import {
  ScrollView,
  StyleSheet,
} from 'react-native';

const PaddedScrollView = function PaddedScrollView(props) {
  return (
    <ScrollView style={styles.wrapper}>
      {props.children}
    </ScrollView>
  );
};

PaddedScrollView.defaultProps = {
  children: [],
};

export default PaddedScrollView;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
});
