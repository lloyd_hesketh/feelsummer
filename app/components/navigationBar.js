import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,

} from 'react-native';
import PropTypes from 'prop-types';

import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

import text from '../lib/text.json';
import colors from '../lib/colors.json';

class NavigationBar extends Component {
  handleBackPress = () => {
    Actions.pop();
  }
  handleCartPress = () => {
    if (this.props.routeName !== 'cart') {
      Actions.cart();
    }
  }
  render() {
    console.log('navigationHeader.js', this.props);
    return (
      <View style={styles.wrapper}>
        <View style={styles.navigationWrapper}>
          <TouchableHighlight
            style={styles.backButton}
            onPress={this.handleBackPress}
            underlayColor={colors.pinkOpacity}
          >
            <View style={styles.backChevron}>
              <Icon name="angle-left" size={45} color="#FFF" />
            </View>
          </TouchableHighlight>
          <View style={styles.titleWrapper}>
            <Text style={styles.text} numberOfLines={1}>{this.props.text}</Text>
          </View>
          <View style={styles.rightPadding}>
            {
              this.props.cartItemCount > 0
              ? <TouchableHighlight
                style={styles.cartButton}
                onPress={this.handleCartPress}
                underlayColor="white"
              >
                <Text style={styles.cartText}>
                  {this.props.cartItemCount}
                  <Icon name="shopping-basket" size={20} color={colors.pink} />
                </Text>
              </TouchableHighlight>
              : null
            }
          </View>
        </View>
      </View>
    );
  }
}

NavigationBar.propTypes = {
  text: PropTypes.string.isRequired,
};

export default connect(state => ({
  cartItemCount: state.cart.items.length,
}),
)(NavigationBar);

let styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.pink,
    flexDirection: 'column',
    justifyContent: 'center',
    height: 70,
    padding: 10,
    paddingTop: 20,
  },
  navigationWrapper: {
    flex: 1,
    flexDirection: 'row',
  },
  backButton: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  titleWrapper: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  rightPadding: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  text: {
    ...text,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  cartButton: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 5,
  },
  cartText: {
    ...text,
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    color: colors.pink,
  },
  backChevron: {
    padding: 10,
  },
});
