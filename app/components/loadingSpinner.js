import React from 'react';

import {
  View,
  StyleSheet,
} from 'react-native';

import Spinner from 'react-native-spinkit';

const LoadingSpinner = function LoadingSpinner(props) {
  return (
    <View style={styles.wrapper}>
      <Spinner size={props.size} type={props.type} color={props.color}/>
    </View>
  );
};

export default LoadingSpinner;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
