import React from 'react';

import {
  View,
  StyleSheet,
  TouchableHighlight,
  Text,
} from 'react-native';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

const ModalButton = function ModalButton(props) {
  return (
    <View style={styles.wrapper}>
      <TouchableHighlight
        onPress={props.onPress}
        style={styles.button}
      >
        <Text style={styles.text}>Confirm</Text>
      </TouchableHighlight>
    </View>
  );
};

export default ModalButton;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#FFF',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  button: {
    margin: 20,
    padding: 20,
    alignItems: 'center',
    backgroundColor: colors.pink,
  },
  text: {
    ...text,
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
