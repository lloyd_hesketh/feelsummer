import React from 'react';

import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

const ViewEventTitle = function ViewEventTitle(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
};

export default ViewEventTitle;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.sea,
    padding: 20,
  },
  text: {
    ...text,
    color: '#FFF',
    fontWeight: '800',
    fontSize: 20,
  },
});
