import React from 'react';

import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
} from 'react-native';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

const BookNowButton = function BookNowButton(props) {
  return (
    <TouchableHighlight onPress={props.onPress}>
      <View style={styles.wrapper}>
        <Text style={styles.text}>{props.text || 'BUY NOW'}</Text>
      </View>
    </TouchableHighlight>
  );
};

export default BookNowButton;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.green,
    alignItems: 'center',
    paddingTop: 20,
    paddingBottom: 20,
    width: '100%',
  },
  text: {
    ...text,
    color: colors.grey,
    fontWeight: '800',
    fontSize: 20,
  },
});
