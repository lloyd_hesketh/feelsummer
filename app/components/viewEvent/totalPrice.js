import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import functions from '../../lib/functions';
import colors from '../../lib/colors.json';
import text from '../../lib/text.json';


const TotalPrice = function TotalPrice(props) {
  return (
    <View style={styles.wrapper}>
      <View style={styles.totalPricePerTicketWrapper}>
        <Text style={styles.totalPriceText}>TOTAL PRICE PER TICKET:</Text>
        <Text style={styles.totalPriceValueText}>{functions.toCurrency(props.resortPrice)}</Text>
      </View>
      <View style={styles.explainWrapper}>
        <Text style={styles.explainText}>
          <Icon name="info-circle" color="#FFF" />
          {` Pay ${functions.toCurrency(props.toPay)} deposit and then the remaining balance of ${functions.toCurrency(props.totalPrice)} in resort.`}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  totalPricePerTicketWrapper: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#FFF',
  },
  totalPriceText: {
    ...text,
    fontWeight: 'bold',
    color: colors.green,
  },
  totalPriceValueText: {
    ...text,
    fontWeight: 'bold',
    color: '#FFF',
    paddingLeft: 5,
  },
  explainWrapper: {
    backgroundColor: '#7B6D6C',
    marginTop: 5,
    marginBottom: 5,
    padding: 5,
  },
  explainText: {
    ...text,
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 15,
  },
  infoIconWrapper: {
    marginRight: 2,
  },
});

export default TotalPrice;
