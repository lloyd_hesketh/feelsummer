import React from 'react';

import {
  View,
  StyleSheet,
  Image,
} from 'react-native';

const ViewEventImage = function ViewEventImage(props) {
  const secureUri = props.src.replace('http://', 'https://');
  return (
    <View>
      <Image
        style={styles.image}
        source={{ uri: secureUri }}
      />
    </View>
  );
};

export default ViewEventImage;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 180,
  },
});
