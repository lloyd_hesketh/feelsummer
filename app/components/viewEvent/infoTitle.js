import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import text from '../../lib/text.json';
import colors from '../../lib/colors.json';

const InfoTitle = function InfoTitle(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{props.text}</Text>
    </View>
  );
};

export default InfoTitle;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: colors.sea,
    padding: 20,
    marginTop: 10,
  },
  text: {
    ...text,
    color: '#FFF',
    fontWeight: 'bold',
    fontSize: 20,
  },
});
