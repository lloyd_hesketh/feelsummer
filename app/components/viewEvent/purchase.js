import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  View,
  StyleSheet,
} from 'react-native';

import * as viewEventActions from '../../actions/viewEventActions';
import * as cartActions from '../../actions/cartActions';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';

import BuyNowButton from './buyNowButton';
import DisplayPickerButton from './displayPickerButton';
import Price from './price';
import TotalPrice from './totalPrice';

const ViewEventPurchaseArea = function ViewEventPurchaseArea(props) {
  const calculatedPrice = props.event.price * props.selectedQuantity;
  const calculatedTotal = (props.event.regular_price * props.selectedQuantity) - calculatedPrice;
  return (
    <View style={styles.wrapper}>
      <View style={styles.pickerWrapper}>
        <DisplayPickerButton
          text={props.selectedDate.format('ddd Do MMMM YYYY')}
          onPress={props.viewEventActions.toggleDateModal}
        />
      </View>
      <View style={styles.pickerWrapper}>
        <DisplayPickerButton
          text={`${props.selectedQuantity} Ticket${props.selectedQuantity > 1 ? 's' : ''}`}
          onPress={props.viewEventActions.toggleQuantityModal}
        />
      </View>
      <View style={styles.priceWrapper}>
        <Price price={props.event.price} />
      </View>
      <View>
        <TotalPrice
          price={props.event.price}
          resortPrice={props.event.regular_price}
          totalPrice={calculatedTotal}
          toPay={calculatedPrice}
        />
      </View>
      <BuyNowButton
        onPress={
          () =>
            props.cartActions.addEventToCart(
              props.event,
              props.selectedQuantity,
              props.selectedDate
          )
        }
      />
    </View>
  );
};

export default connect(state => ({
}),
dispatch => ({
  viewEventActions: bindActionCreators(viewEventActions, dispatch),
  cartActions: bindActionCreators(cartActions, dispatch),
}),
)(ViewEventPurchaseArea);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'stretch',
    backgroundColor: colors.grey,
    padding: 20,
  },
  text: {
    ...text,
    color: '#FFF',
    fontWeight: '800',
    fontSize: 20,
  },
  pickerWrapper: {
    marginBottom: 10,
  },
  priceWrapper: {
    marginBottom: 0,
  },
});
