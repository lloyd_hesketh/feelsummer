import React from 'react';

import {
  View,
  StyleSheet,
  Text,
  TouchableHighlight,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

// import colors from '../../lib/colors.json';
import text from '../../lib/text.json';
// import functions from '../../lib/functions';

const DisplayPickerButton = function DisplayPickerButton(props) {
  return (
    <View style={styles.wrapper}>
      <TouchableHighlight
        style={styles.touchableHighlight}
        underlayColor="#e6e6e6"
        onPress={props.onPress}
      >
        <View style={styles.pickerWrapper}>
          <Text style={styles.text}>{props.text}</Text>
          <View style={styles.chevronWrapper}>
            <Icon name="chevron-down" />
          </View>
        </View>
      </TouchableHighlight>
    </View>
  );
};

export default DisplayPickerButton;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  touchableHighlight: {
    flex: 1,
  },
  pickerWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  text: {
    ...text,
    fontSize: 18,
    fontWeight: 'bold',
  },
  chevronWrapper: {
    paddingTop: 4,
  },
});
