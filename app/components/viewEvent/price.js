import React from 'react';

import {
  View,
  StyleSheet,
  Text,
} from 'react-native';

import colors from '../../lib/colors.json';
import text from '../../lib/text.json';
import functions from '../../lib/functions';

const ViewEventPrice = function ViewEventPrice(props) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.priceText}>{functions.toCurrency(props.price)}</Text>
    </View>
  );
};

export default ViewEventPrice;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  priceText: {
    ...text,
    color: '#fff',
    fontWeight: '800',
    fontSize: 40,
  },
});
