import React from 'react';

import {
  View,
  StyleSheet,
} from 'react-native';

import CalendarDatePicker from 'react-native-calendar-datepicker';
import Moment from 'moment';

import colors from '../lib/colors.json';
import text from '../lib/text.json';

const Calendar = function Calendar(props) {
  const BLUE = '#2196F3';
  const WHITE = '#FFFFFF';
  const GREY = '#BDBDBD';
  const BLACK = '#424242';
  const LIGHT_GREY = '#F5F5F5';
  return (
    <View>
      <CalendarDatePicker
        style={styles.callendar}
        onChange={date => props.onChange(date)}
        selected={props.selectedDate}
        // We use Moment.js to give the minimum and maximum dates.
        minDate={Moment().startOf('day')}
        maxDate={Moment().add(1, 'years').startOf('day')}
        barView={{
          backgroundColor: colors.pink,
          padding: 10,
        }}
        barText={{
          fontWeight: 'bold',
          color: WHITE,
          ...text,
          fontSize: 20,
        }}
        stageView={{
          padding: 0,
        }}
        dayHeaderView={{
          backgroundColor: LIGHT_GREY,
          borderBottomColor: LIGHT_GREY,
        }}
        dayHeaderText={{
          fontWeight: 'bold',
          color: BLACK,
        }}
        dayRowView={{
          borderColor: LIGHT_GREY,
          height: 40,
        }}
        dayText={{
          color: BLACK,
        }}
        dayDisabledText={{
          color: GREY,
        }}
        dayTodayText={{
          fontWeight: 'bold',
          color: colors.pink,
        }}
        daySelectedText={{
          fontWeight: 'bold',
          backgroundColor: colors.pink,
          color: WHITE,
          borderColor: 'transparent',
          overflow: 'hidden',
        }}
        monthText={{
          color: BLACK,
          borderColor: BLACK,
        }}
        monthDisabledText={{
          color: GREY,
          borderColor: GREY,
        }}
        monthSelectedText={{
          fontWeight: 'bold',
          backgroundColor: BLUE,
          color: WHITE,
          overflow: 'hidden',
        }}
        yearMinTintColor={BLUE}
        yearMaxTintColor={GREY}
        yearText={{
          color: BLACK,
        }}
      />
    </View>
  );
};

export default Calendar;

const styles = StyleSheet.create({
  callendar: {
    backgroundColor: '#FFF',
  },
});
