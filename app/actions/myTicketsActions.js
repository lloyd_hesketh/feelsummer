import * as types from './actionTypes';
import { getStorageItem } from '../lib/localStorageManager';

export function initialiseMyTickets() {
  return (dispatch) => {
    getStorageItem('myTickets')
    .then(data => (
      dispatch({
        type: types.INITIALISE_MYTICKETS,
        payload: data,
      })
    ));
  };
}
