import * as types from './actionTypes';

export function toggleDateModal() {
  return {
    type: types.TOGGLE_DATE_MODAL,
  };
}

export function toggleQuantityModal() {
  return {
    type: types.TOGGLE_QUANTITY_MODAL,
  };
}

export function resetState() {
  return {
    type: types.RESET_VIEWEVENT_STATE,
  };
}

export function setSelectedDate(date) {
  return {
    type: types.SET_SELECTED_DATE,
    payload: date,
  };
}

export function setSelectedQuantity(quantity) {
  return {
    type: types.SET_SELECTED_QUANTITY,
    payload: quantity,
  };
}

export function confirmDate(date) {
  return {
    type: types.SET_CONFIRMED_DATE,
    payload: date,
  };
}

export function confirmQuantity(quantity) {
  return {
    type: types.SET_CONFIRMED_QUANTITY,
    payload: quantity,
  };
}
