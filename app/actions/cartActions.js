import { Actions } from 'react-native-router-flux';

import * as types from './actionTypes';
import { getStorageItem } from '../lib/localStorageManager';

export function initialiseCart() {
  return (dispatch) => {
    getStorageItem('cart')
    .then(data => (
      dispatch({
        type: types.INITIALISE_CART,
        payload: data,
      })
    ));
  };
}

export function addEventToCart(event, quantity, date) {
  return (dispatch) => {
    dispatch({
      type: types.ADD_EVENT_TO_CART,
      payload: { event, quantity, date },
    });
    Actions.cart();
  };
}

export function removeCartItemAtIndex(index) {
  return {
    type: types.REMOVE_EVENT_FROM_CART,
    payload: index,
  };
}
