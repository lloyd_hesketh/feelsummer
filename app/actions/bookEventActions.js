import * as types from './actionTypes';
import urls from '../lib/urls';

function getEventsSuccess(data) {
  return { type: types.GET_EVENTS_SUCCESS, data };
}

function getEventsFailure(error) {
  return { type: types.GET_EVENTS_FAILURE, error };
}

export function getEvents() {
  return (dispatch) => {
    fetch(urls.GET_EVENTS)
    .then(resp => resp.json())
    .then(data => dispatch(getEventsSuccess(data)))
    .catch(error => dispatch(getEventsFailure(error)));
  };
}

export function handleBookEventsUnmount() {
  return { type: types.BOOK_EVENTS_UNMOUNT };
}
