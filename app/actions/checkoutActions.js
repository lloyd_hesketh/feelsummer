import { Actions } from 'react-native-router-flux';
import * as types from './actionTypes';
import urls from '../lib/urls';
import { getStorageItem } from '../lib/localStorageManager';

export function initialiseCheckout() {
  return (dispatch) => {
    getStorageItem('stripeCustomerData')
    .then((data) => {
      console.log(data);
      dispatch({
        type: types.SAVE_STRIPE_CUSTOMER_DATA,
        payload: data,
      })
    }
    );
  };
}

export function stripeCreateCharge(stripeData, amount, description, cartItems) {
  return (dispatch) => {
    const reqHeaders = new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'fs-apiKey': 'e63a8061-4372-4b01-90a1-f1181a2e267e',
    });
    fetch(urls.STRIPE_CREATE_CHARGE, {
      method: 'POST',
      headers: reqHeaders,
      body: JSON.stringify({
        amount,
        stripeToken: stripeData.tokenId,
        description,
      }),
    })
    .then((response) => {
      if (response.status === 200) {
        try {
          return response.json();
        } catch (e) {
          return null;
        }
      }
      throw new Error(`response status was not 200, it was: ${response.status}`);
    })
    .then((data) => {
      if (data && data.paid) {
        dispatch({
          type: types.SAVE_CART_TO_MY_TICKETS,
          payload: cartItems,
        });
        dispatch({
          type: types.STRIPE_CHARGE_SUCCESS,
          payload: data,
        });
        dispatch({
          type: types.CHANGE_CHECKOUT_STATUS,
          payload: null,
        });
        Actions.paymentComplete();
      } else {
        dispatch({
          type: types.CHANGE_CHECKOUT_STATUS,
          payload: 'error',
        });
        throw new Error('Payment not complete');
      }
    })
    .catch((error) => {
      console.log('error', error);
      dispatch({
        type: types.CHANGE_CHECKOUT_STATUS,
        payload: 'error',
      });
      dispatch({ type: types.STRIPE_CHARGE_ERROR });
    });
  };
}

export function stripeCreateCustomerCharge(stripeData, amount, description, cartItems) {
  return (dispatch) => {
    const reqHeaders = new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'fs-apiKey': 'e63a8061-4372-4b01-90a1-f1181a2e267e',
    });
    fetch(urls.STRIPE_CREATE_CUSTOMER_CHARGE, {
      method: 'POST',
      headers: reqHeaders,
      body: JSON.stringify({
        amount,
        customerId: stripeData.tokenId,
        description,
      }),
    })
    .then((response) => {
      if (response.status === 200) {
        try {
          return response.json();
        } catch (e) {
          return null;
        }
      }
      throw new Error(`response status was not 200, it was: ${response.status}`);
    })
    .then((data) => {
      if (data && data.paid) {
        dispatch({
          type: types.SAVE_CART_TO_MY_TICKETS,
          payload: cartItems,
        });
        dispatch({
          type: types.STRIPE_CHARGE_SUCCESS,
          payload: data,
        });
        dispatch({
          type: types.CHANGE_CHECKOUT_STATUS,
          payload: null,
        });
        Actions.paymentComplete();
      } else {
        dispatch({
          type: types.CHANGE_CHECKOUT_STATUS,
          payload: 'error',
        });
        throw new Error('Payment not complete');
      }
    })
    .catch((error) => {
      console.log('error', error);
      dispatch({
        type: types.CHANGE_CHECKOUT_STATUS,
        payload: 'error',
      });
      dispatch({ type: types.STRIPE_CHARGE_ERROR });
    });
  };
}

export function stripeCreateCustomer(stripeData, email) {
  return (dispatch) => {
    const reqHeaders = new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'fs-apiKey': 'e63a8061-4372-4b01-90a1-f1181a2e267e',
    });
    fetch(urls.STRIPE_CREATE_CUSTOMER, {
      method: 'POST',
      headers: reqHeaders,
      body: JSON.stringify({
        stripeToken: stripeData.tokenId,
        email,
      }),
    })
    .then((response) => {
      if (response.status === 200) {
        try {
          return response.json();
        } catch (e) {
          return null;
        }
      }
      throw new Error(`response status was not 200, it was: ${response.status}`);
    })
    .then((data) => {
      if (data) {
        dispatch({
          type: types.SAVE_STRIPE_CUSTOMER_DATA,
          payload: data,
        });
      } else {
        dispatch({
          type: types.CHANGE_CHECKOUT_STATUS,
          payload: 'error',
        });
        throw new Error('Payment not complete');
      }
    })
    .catch((error) => {
      console.log('error', error);
      dispatch({
        type: types.CHANGE_CHECKOUT_STATUS,
        payload: 'error',
      });
      dispatch({ type: types.STRIPE_CHARGE_ERROR });
    });
  };
}

export function updateStripeToken(stripeToken) {
  return {
    type: types.UPDATE_STRIPE_TOKEN,
    payload: stripeToken,
  };
}

export function updateCheckoutStatus(value) {
  return {
    type: types.CHANGE_CHECKOUT_STATUS,
    payload: value,
  };
}

export function updateBillingDetails(details) {
  return {
    type: types.UPDATE_BILLING_DETAILS,
    payload: details,
  };
}
