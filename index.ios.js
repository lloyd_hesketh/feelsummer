/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import { AppRegistry } from 'react-native';
import AppWrapper from './app/containers/appWrapper';

AppRegistry.registerComponent('FeelSummer', () => AppWrapper);
